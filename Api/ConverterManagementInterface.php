<?php

namespace TestTask\CurrencyConverter\Api;

interface ConverterManagementInterface
{
    /**
     * Convert currencies. Connect to api, get exchange rate and calculate second currency value
     *
     * @param mixed $value
     * @param string $currencyFrom
     * @param string $currencyTo
     * @throws TestTask\CurrencyConverter\Exception\BadApiResponseException
     * @throws TestTask\CurrencyConverter\Exception\InvalidApiUrlException
     * @return float
     */
    public function convert($value, string $currencyFrom, string $currencyTo) : float;
     
    /**
     * Check passed value is positive float value
     * @param mixed $value
     * @throws TestTask\CurrencyConverter\Exception\InvalidCurrencyValueException
     * @return boolean
     */
    public function isValid($value) : bool;
}