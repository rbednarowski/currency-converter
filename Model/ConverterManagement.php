<?php

namespace TestTask\CurrencyConverter\Model;

use TestTask\CurrencyConverter\Api\ConverterManagementInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use TestTask\CurrencyConverter\Exception\BadApiResponseException;
use TestTask\CurrencyConverter\Exception\InvalidApiUrlException;
use TestTask\CurrencyConverter\Exception\InvalidCurrencyValueException;

class ConverterManagement implements ConverterManagementInterface
{
    const CURRENCY_CONVERTER_API_URL = 'http://free.currencyconverterapi.com/api/v5/convert?compact=ultra';

    protected $curlClient;
    protected $jsonHelper;
    
    public function __construct(Curl $curl, JsonHelper $jsonHelper)
    {
        $this->curlClient = $curl;
        $this->jsonHelper = $jsonHelper;
    }

    public function convert($currencyValue, string $currencyFrom = 'RUB', string $currencyTo = 'PLN') : float
    {        
        if (false === $this->isValid($currencyValue)) {
            throw new InvalidCurrencyValueException('Sorry, currency must be a positive number, try again');
        }

        $currenciesConverterAPIUrl = $this->_getCurrenciesConverterAPIUrl($currencyFrom, $currencyTo);
        $this->curlClient->addHeader('Content-Type', 'application/json');

        try {
            $this->curlClient->get($currenciesConverterAPIUrl);
        } catch (\Exception $e) {
            throw new InvalidApiUrlException('Invalid Converter Api url');
        }

        try {
            $currenciesRates = $this->jsonHelper->jsonDecode($this->curlClient->getBody());
        } catch (\Exception $e) {
            throw new BadApiResponseException('Converter Api response is not valid');
        }

        if (true === is_array($currenciesRates) && true === isset($currenciesRates[$currencyFrom . '_' . $currencyTo])) {
            return floatval($currenciesRates[$currencyFrom . '_' . $currencyTo]) * $currencyValue;
        }
                        
        throw new BadApiResponseException('Converter Api response is not valid');
    }

    public function isValid($currencyValue) : bool
    {
        if (false === is_numeric($currencyValue) || $currencyValue < 0) {            
            return false;
        }
        
        return true;
    }

    private function _getCurrenciesConverterAPIUrl(string $currencyFrom, string $currencyTo) : string
    {
        return self::CURRENCY_CONVERTER_API_URL . '&q=' . $currencyFrom . '_' . $currencyTo;
    }
}
